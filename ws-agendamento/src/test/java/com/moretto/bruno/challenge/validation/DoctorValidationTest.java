package com.moretto.bruno.challenge.validation;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class DoctorValidationTest {

    @Mock
    private DoctorRepository doctorRepository;

    @InjectMocks
    private DoctorValidation doctorValidation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldThrowAValidationErrorWhenEmailAlreadyExists() {
        when(doctorRepository.existsByEmail(anyString()))
                .thenReturn(Boolean.TRUE);

        ValidationException exception =  assertThrows(
                ValidationException.class,
                () -> doctorValidation.mailAlreadyExists("test@gmail.com")
        );

        assertThat(exception).hasMessageContaining("Já existe um login cadastrado com este e-mail");
    }

    @Test
    void shouldThrowAValidationErrorWhenPasswrodsEquals() {
        ValidationException exception =  assertThrows(
                ValidationException.class,
                () -> doctorValidation.passwordsEquals("test", "test123")
        );

        assertThat(exception).hasMessageContaining("Senhas são diferentes");
    }

    @Test
    void shouldThrowAValidationErrorWhenDoctorDoesNotExists() {
        when(doctorRepository.findByEmail(anyString()))
                .thenReturn(null);

        ValidationException exception =  assertThrows(
                ValidationException.class,
                () -> doctorValidation.doctorWithMailExists("test@gmail.com")
        );

        assertThat(exception).hasMessageContaining("Médico não existe");
    }

}