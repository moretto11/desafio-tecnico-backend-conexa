package com.moretto.bruno.challenge.mapper;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.dto.PatientDto;
import com.moretto.bruno.challenge.dto.ScheduleDto;
import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.entity.Schedule;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class ScheduleMapperTest {

    private static final Long TEST_ID = 123L;
    private static final String TEST_CPF = "123.456.789.11";
    private static final String TEST_NAME = "Fulano da Silva";
    private static final LocalDateTime TEST_DATE = LocalDateTime.now();


    @Test
    void shouldMapAnEntityToADto() {
        Schedule entity = Schedule.builder()
                .id(TEST_ID)
                .patientCpf(TEST_CPF)
                .patientName(TEST_NAME)
                .date(TEST_DATE)
                .build();

        ScheduleDto dto = ScheduleMapper.toDto(entity);

        assertEquals(TEST_ID, dto.getId());
        assertEquals(TEST_CPF, dto.getPatient().getPatientCpf());
        assertEquals(TEST_NAME, dto.getPatient().getPatientName());
        assertEquals(TEST_DATE, dto.getDate());
    }

    @Test
    void shouldMapADtoToAnEntity() {
        ScheduleDto dto = ScheduleDto.builder()
                .id(TEST_ID)
                .patient(PatientDto.builder()
                        .patientCpf(TEST_CPF)
                        .patientName(TEST_NAME)
                        .build())
                .date(TEST_DATE)
                .build();

        Schedule entity = ScheduleMapper.toEntity(dto);

        assertEquals(TEST_ID, entity.getId());
        assertEquals(TEST_CPF, entity.getPatientCpf());
        assertEquals(TEST_NAME, entity.getPatientName());
        assertEquals(TEST_DATE, entity.getDate());
    }


}