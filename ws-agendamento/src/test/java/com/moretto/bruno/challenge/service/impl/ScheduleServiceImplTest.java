package com.moretto.bruno.challenge.service.impl;

import com.moretto.bruno.challenge.dto.PatientDto;
import com.moretto.bruno.challenge.dto.ScheduleDto;
import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.entity.Schedule;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.mapper.ScheduleMapper;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import com.moretto.bruno.challenge.repository.ScheduleRepository;
import com.moretto.bruno.challenge.validation.ScheduleValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class ScheduleServiceImplTest {

    @Mock
    private ScheduleRepository scheduleRepository;

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private ScheduleValidation scheduleValidation;

    @Mock
    private WebRequest request;

    @InjectMocks
    private ScheduleServiceImpl scheduleService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        when(request.getHeader(anyString()))
                .thenReturn("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtZWRpY29AZW1haWwuY29tIiwiZXhwIjoxNjQ5MDIwMjQ5fQ.e1g2a2M67pmGSIeoBPpuEufByz3hZCmEa183HLyVIXxj53-VhaBEN7zBSLCmklehHGaB4-uJVSbkR8nN5DmNgA");
    }

    @Test
    void shouldSaveSuccessful() throws ValidationException {
        ScheduleDto dto = ScheduleDto.builder()
                .patient(PatientDto.builder()
                        .patientCpf("123.456.789-11")
                        .patientName("Fulano da Silva")
                        .build())
                .date(LocalDateTime.now())
                .build();

        Schedule entity = ScheduleMapper.toEntity(dto);
        entity.setId(123L);

        Doctor doctor = Doctor.builder()
                .id(123L)
                .email("test@gmail.com")
                .build();
        entity.setDoctor(doctor);

        doNothing().when(scheduleValidation).validationSuite(any(ScheduleDto.class), anyString());
        when(doctorRepository.findByEmail(anyString()))
                .thenReturn(doctor);
        when(scheduleRepository.save(any(Schedule.class)))
                .thenReturn(entity);

        dto = scheduleService.save(dto);

        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getDate(), dto.getDate());
        assertNotNull(entity.getDoctor().getId());
        verify(scheduleRepository, atMostOnce()).save(any(Schedule.class));
    }

    @Test
    void shouldThrowAnValidationExceptionWhenDtoIsInvalid() throws ValidationException {
        doThrow(ValidationException.class).when(scheduleValidation)
                .validationSuite(any(ScheduleDto.class), anyString());

        ValidationException exception = assertThrows(ValidationException.class,
                () -> scheduleService.save(new ScheduleDto()));

        verify(scheduleRepository, never()).save(any(Schedule.class));
    }

}