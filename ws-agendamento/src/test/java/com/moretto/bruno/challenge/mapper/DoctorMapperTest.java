package com.moretto.bruno.challenge.mapper;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.entity.Doctor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoctorMapperTest {

    private static final Long TEST_ID = 123L;
    private static final String TEST_CPF = "123.456.789.11";

    @Test
    void shouldMapAnEntityToADto() {
        Doctor entity = Doctor.builder()
                .id(TEST_ID)
                .cpf(TEST_CPF)
                .build();

        DoctorDto dto = DoctorMapper.toDto(entity);

        assertEquals(TEST_ID, dto.getId());
        assertEquals(TEST_CPF, dto.getCpf());
    }

    @Test
    void shouldMapADtoToAnEntity() {
        DoctorDto dto = DoctorDto.builder()
                .id(TEST_ID)
                .cpf(TEST_CPF)
                .build();

        Doctor entity = DoctorMapper.toEntity(dto);

        assertEquals(TEST_ID, entity.getId());
        assertEquals(TEST_CPF, entity.getCpf());
    }

}