package com.moretto.bruno.challenge.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JWTUtilsTest {

    @Test
    void shouldReturnJWTSubject() {
        String jwtSubject = JWTUtils.getJWTSubject(
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9."+
                "TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQo"
        );
        assertEquals("1234567890", jwtSubject);
    }

}