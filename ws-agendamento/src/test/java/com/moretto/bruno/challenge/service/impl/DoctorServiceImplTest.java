package com.moretto.bruno.challenge.service.impl;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.mapper.DoctorMapper;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import com.moretto.bruno.challenge.validation.DoctorValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class DoctorServiceImplTest {

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private DoctorValidation doctorValidation;

    @InjectMocks
    private DoctorServiceImpl doctorService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        when(bCryptPasswordEncoder.encode(anyString()))
                .thenReturn("3foisajd#@!fidosaj#!afjiasdj00031FAD1");
    }

    @Test
    void shouldSaveSuccessfulANewDoctor() throws ValidationException {
        DoctorDto dto = DoctorDto.builder()
                .email("test@gmail.com")
                .password("test123")
                .confirmationPassword("test123")
                .speciality("test")
                .cpf("123.456.789-11")
                .birthDate(LocalDate.now())
                .phone("(51) 999889-988")
                .build();
        Doctor entity = DoctorMapper.toEntity(dto);
        entity.setId(123L);
        entity.setPassword("3foisajd#@!fidosaj#!afjiasdj00031FAD1");

        doNothing().when(doctorValidation).validationSuite(dto);
        when(doctorRepository.save(any(Doctor.class))).thenReturn(entity);

        dto = doctorService.save(dto);

        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getCpf(), dto.getCpf());
        assertNotEquals("test123", dto.getPassword());
        verify(doctorRepository, atMostOnce()).save(entity);
    }

    @Test
    void shouldThrowsAnValidationExceptionWhenDtoIsInvalid() throws ValidationException {
        doThrow(ValidationException.class).when(doctorValidation)
                .validationSuite(any(DoctorDto.class));

        ValidationException exception = assertThrows(ValidationException.class,
                () -> doctorService.save(new DoctorDto()));

        verify(doctorRepository, never()).save(any(Doctor.class));
    }

}