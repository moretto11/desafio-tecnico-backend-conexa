package com.moretto.bruno.challenge.validation;

import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.repository.ScheduleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class ScheduleValidationTest {

    @Mock
    private ScheduleRepository scheduleRepository;

    @Mock
    private DoctorValidation doctorValidation;

    @InjectMocks
    private ScheduleValidation scheduleValidation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldThrowAValidationErrorWhenDateIsInPast() {
        LocalDateTime pastDate = LocalDateTime.now().minusDays(2L);

        ValidationException exception = assertThrows(
                ValidationException.class,
                () -> scheduleValidation.dateIsNotInPast(pastDate));

        assertThat(exception).hasMessageContaining("O horário do agendamento está no passado");
    }

    @Test
    void shouldThrowAValidationErrorWhenAlreadyExistsAScheduleOnThatDate() {
        when(scheduleRepository.existsByDoctor_EmailAndDate(anyString(), any(LocalDateTime.class)))
                .thenReturn(Boolean.TRUE);

        ValidationException exception = assertThrows(
                ValidationException.class,
                () -> scheduleValidation.alreadyExistsAScheduleInTheDateAndHour(
                        "test@gmail.com", LocalDateTime.now())
        );

        assertThat(exception).hasMessageContaining("Já existe uma consulta agendada neste horário");
    }


}