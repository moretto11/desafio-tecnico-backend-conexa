package com.moretto.bruno.challenge.service.impl;

import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class UserDetailsServiceImplTest {

    @Mock
    private DoctorRepository doctorRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldReturnAnUserDetailsObjectSuccessful() {
        Doctor doctor = Doctor.builder()
                .email("test@gmail.com")
                .password("test123")
                .build();

        when(doctorRepository.findByEmail(anyString()))
                .thenReturn(doctor);

        UserDetails userDetails = userDetailsService.loadUserByUsername("test@gmail.com");

        assertEquals("test@gmail.com", userDetails.getUsername());
        assertEquals("test123", userDetails.getPassword());
    }

    @Test
    void shouldThrowAnExceptionIfDoctorDoestExists() {
        when(doctorRepository.findByEmail(anyString()))
                .thenReturn(null);

        UsernameNotFoundException exception = assertThrows(UsernameNotFoundException.class,
                () -> userDetailsService.loadUserByUsername("idn@gmail.com"));

        verify(doctorRepository, atMostOnce()).findByEmail(anyString());
    }

}