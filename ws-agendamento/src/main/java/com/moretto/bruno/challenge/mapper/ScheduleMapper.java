package com.moretto.bruno.challenge.mapper;

import com.moretto.bruno.challenge.dto.PatientDto;
import com.moretto.bruno.challenge.dto.ScheduleDto;
import com.moretto.bruno.challenge.entity.Schedule;

public class ScheduleMapper {

    private ScheduleMapper() {
    }

    public static ScheduleDto toDto(Schedule entity) {
        return ScheduleDto.builder()
                .id(entity.getId())
                .patient(PatientDto.builder()
                        .patientCpf(entity.getPatientCpf())
                        .patientName(entity.getPatientName())
                        .build())
                .date(entity.getDate())
                .build();
    }

    public static Schedule toEntity(ScheduleDto dto) {
        return Schedule.builder()
                .id(dto.getId())
                .patientName(dto.getPatient().getPatientName())
                .patientCpf(dto.getPatient().getPatientCpf())
                .date(dto.getDate())
                .build();
    }

}
