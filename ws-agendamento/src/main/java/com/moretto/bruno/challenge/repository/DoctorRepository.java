package com.moretto.bruno.challenge.repository;

import com.moretto.bruno.challenge.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    Doctor findByEmail(String mail);

    Boolean existsByEmail(String mail);

}