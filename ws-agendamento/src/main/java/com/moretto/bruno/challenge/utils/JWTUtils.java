package com.moretto.bruno.challenge.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JWTUtils {

    private JWTUtils() {
    }

    public static String getJWTSubject(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getSubject();
    }


}
