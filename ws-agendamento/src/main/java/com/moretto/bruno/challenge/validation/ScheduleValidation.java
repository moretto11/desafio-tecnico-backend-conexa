package com.moretto.bruno.challenge.validation;

import com.moretto.bruno.challenge.dto.ScheduleDto;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ScheduleValidation {

    private final ScheduleRepository scheduleRepository;
    private final DoctorValidation doctorValidation;

    public void validationSuite(ScheduleDto dto, String doctorMail) throws ValidationException {
        dateIsNotInPast(dto.getDate());
        alreadyExistsAScheduleInTheDateAndHour(doctorMail, dto.getDate());
        doctorValidation.doctorWithMailExists(doctorMail);
    }

    protected void dateIsNotInPast(LocalDateTime date) throws ValidationException {
        if(date.isBefore(LocalDateTime.now())) {
            throw new ValidationException("O horário do agendamento está no passado");
        }
    }

    protected void alreadyExistsAScheduleInTheDateAndHour(String doctorMail, LocalDateTime date) throws ValidationException {
        if(Boolean.TRUE.equals(scheduleRepository.existsByDoctor_EmailAndDate(doctorMail, date))) {
            throw new ValidationException("Já existe uma consulta agendada neste horário");
        }
    }

}
