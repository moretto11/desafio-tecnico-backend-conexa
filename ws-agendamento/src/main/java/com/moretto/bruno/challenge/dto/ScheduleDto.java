package com.moretto.bruno.challenge.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ScheduleDto extends RepresentationModel<ScheduleDto> {

    private Long id;

    @Valid
    @NotNull(message = "É preciso informar o paciente da consulta")
    @JsonAlias("paciente")
    private PatientDto patient;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "É preciso informar o horário do agendamento")
    @JsonAlias("dataHora")
    private LocalDateTime date;

}
