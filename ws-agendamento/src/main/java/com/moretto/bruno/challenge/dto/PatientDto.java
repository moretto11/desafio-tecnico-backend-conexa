package com.moretto.bruno.challenge.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatientDto {

    @NotBlank(message = "É preciso informar o nome do paciente")
    @JsonAlias("nome")
    private String patientName;

    @NotBlank(message = "É preciso informar o CPF do paciente")
    @Size(min = 14, max = 14, message = "CPF com formato inválido. O formato deve ser 000.000.000-00")
    @JsonAlias("cpf")
    private String patientCpf;

}
