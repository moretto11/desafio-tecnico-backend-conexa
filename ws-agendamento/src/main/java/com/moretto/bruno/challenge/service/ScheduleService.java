package com.moretto.bruno.challenge.service;

import com.moretto.bruno.challenge.dto.ScheduleDto;
import com.moretto.bruno.challenge.exception.ValidationException;

public interface ScheduleService {

    /**
     * Cria um agendamento na base
     * @param dto Dados para o agendamento
     * @return DTO do agendamento salvo
     * @throws ValidationException É lançada caso tenha algum dado inválido
     */
    ScheduleDto save(ScheduleDto dto) throws ValidationException;

}
