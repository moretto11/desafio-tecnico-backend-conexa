package com.moretto.bruno.challenge.auth;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class BlacklistTokenStorage {

    private static final Set<String> blacklist = new HashSet<>();

    public Boolean checkIfTokenIsBlacklisted(String token) {
        return blacklist.contains(token);
    }

    public void addTokenToBlacklist(String token) {
        blacklist.add(token);
    }

}
