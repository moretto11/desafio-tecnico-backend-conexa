package com.moretto.bruno.challenge.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "DOCTOR")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "email", length = 120)
    private String email;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Column(name = "speciality", nullable = false, length = 120)
    private String speciality;

    @Column(name = "cpf", nullable = false, length = 11)
    private String cpf;

    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @Column(name = "phone", nullable = false, length = 11)
    private String phone;

}