package com.moretto.bruno.challenge.mapper;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.entity.Doctor;

public class DoctorMapper {

    private DoctorMapper() {
    }

    public static Doctor toEntity(DoctorDto dto) {
        return Doctor.builder()
                .id(dto.getId())
                .cpf(dto.getCpf())
                .birthDate(dto.getBirthDate())
                .speciality(dto.getSpeciality())
                .phone(dto.getPhone())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .build();
    }

    public static DoctorDto toDto(Doctor entity) {
        return DoctorDto.builder()
                .id(entity.getId())
                .cpf(entity.getCpf())
                .birthDate(entity.getBirthDate())
                .speciality(entity.getSpeciality())
                .phone(entity.getPhone())
                .email(entity.getEmail())
                .password(entity.getPassword())
                .build();
    }

}
