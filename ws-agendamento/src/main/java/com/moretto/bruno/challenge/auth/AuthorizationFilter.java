package com.moretto.bruno.challenge.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    private final BlacklistTokenStorage blacklistTokenStorage;

    public AuthorizationFilter(AuthenticationManager authenticationManager, BlacklistTokenStorage blacklistTokenStorage) {
        super(authenticationManager);
        this.blacklistTokenStorage = blacklistTokenStorage;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String token = req.getHeader(Constants.AUTHORIZATION_HEADER);

        if (Objects.isNull(token) || !token.startsWith(Constants.TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(token);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        if (Objects.nonNull(token)) {
            token = token.replace(Constants.TOKEN_PREFIX, "");
            String user = JWT.require(Algorithm.HMAC512(Constants.SECRET.getBytes()))
                    .build()
                    .verify(token)
                    .getSubject();

            if (Objects.nonNull(user) &&
                    Boolean.FALSE.equals(blacklistTokenStorage.checkIfTokenIsBlacklisted(token))) {
                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }

            return null;
        }

        return null;
    }

}
