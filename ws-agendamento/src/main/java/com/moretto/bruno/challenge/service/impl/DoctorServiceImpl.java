package com.moretto.bruno.challenge.service.impl;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import com.moretto.bruno.challenge.service.DoctorService;
import com.moretto.bruno.challenge.validation.DoctorValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static com.moretto.bruno.challenge.mapper.DoctorMapper.toEntity;
import static com.moretto.bruno.challenge.mapper.DoctorMapper.toDto;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final DoctorValidation doctorValidation;

    @Override
    public DoctorDto save(DoctorDto dto) throws ValidationException {
        doctorValidation.validationSuite(dto);

        Doctor entity = toEntity(dto);
        String encodePassword = bCryptPasswordEncoder.encode(entity.getPassword());
        entity.setPassword(encodePassword);

        entity = doctorRepository.save(entity);

        return toDto(entity);
    }

}
