package com.moretto.bruno.challenge.resource;

import com.moretto.bruno.challenge.auth.BlacklistTokenStorage;
import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.service.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping(path = "/v1")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthController {

    private final WebRequest request;
    private final BlacklistTokenStorage blacklistTokenStorage;
    private final DoctorService doctorService;

    @PostMapping(path = "/signup")
    public ResponseEntity<DoctorDto> saveANewDoctor(@Valid @RequestBody DoctorDto dto) throws ValidationException {
        return ResponseEntity.status(HttpStatus.CREATED).body(doctorService.save(dto));
    }

    @PostMapping(path = "/logoff")
    public ResponseEntity<?> logoff() {
        String token = Objects.requireNonNull(request.getHeader("Authorization"))
                .replace("Bearer ", "");
        blacklistTokenStorage.addTokenToBlacklist(token);

        return ResponseEntity.noContent().build();
    }

}
