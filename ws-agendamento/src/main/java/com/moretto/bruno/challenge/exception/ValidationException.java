package com.moretto.bruno.challenge.exception;

public class ValidationException extends Exception {

    public ValidationException(String message) {
        super(message);
    }

}
