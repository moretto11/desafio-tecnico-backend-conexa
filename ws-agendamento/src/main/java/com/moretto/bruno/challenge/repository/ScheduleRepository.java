package com.moretto.bruno.challenge.repository;

import com.moretto.bruno.challenge.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    Boolean existsByDoctor_EmailAndDate(String doctorMail, LocalDateTime date);

}