package com.moretto.bruno.challenge.validation;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DoctorValidation {

    private final DoctorRepository doctorRepository;

    public void validationSuite(DoctorDto dto) throws ValidationException {
        mailAlreadyExists(dto.getEmail());
        passwordsEquals(dto.getPassword(), dto.getConfirmationPassword());
    }

    protected void mailAlreadyExists(String mail) throws ValidationException{
        if(Boolean.TRUE.equals(doctorRepository.existsByEmail(mail))) {
            throw new ValidationException("Já existe um login cadastrado com este e-mail");
        }
    }

    protected void passwordsEquals(String password, String confirmationPassword) throws ValidationException {
        if(!password.equals(confirmationPassword)) {
            throw new ValidationException("Senhas são diferentes");
        }
    }

    protected void doctorWithMailExists(String mail) throws ValidationException {
        if(Objects.isNull(doctorRepository.findByEmail(mail))) {
            throw new ValidationException("Médico não existe");
        }
    }

}
