package com.moretto.bruno.challenge.service.impl;

import com.moretto.bruno.challenge.dto.ScheduleDto;
import com.moretto.bruno.challenge.entity.Doctor;
import com.moretto.bruno.challenge.entity.Schedule;
import com.moretto.bruno.challenge.exception.ValidationException;
import com.moretto.bruno.challenge.repository.DoctorRepository;
import com.moretto.bruno.challenge.repository.ScheduleRepository;
import com.moretto.bruno.challenge.service.ScheduleService;
import com.moretto.bruno.challenge.utils.JWTUtils;
import com.moretto.bruno.challenge.validation.ScheduleValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.WebRequest;

import java.util.Objects;

import static com.moretto.bruno.challenge.mapper.ScheduleMapper.toDto;
import static com.moretto.bruno.challenge.mapper.ScheduleMapper.toEntity;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduleRepository scheduleRepository;
    private final DoctorRepository doctorRepository;
    private final ScheduleValidation scheduleValidation;
    private final WebRequest request;

    @Override
    public ScheduleDto save(ScheduleDto dto) throws ValidationException {
        String authToken = Objects.requireNonNull(request.getHeader("Authorization"))
                .replace("Bearer ", "");
        String doctorMail = JWTUtils.getJWTSubject(authToken);

        scheduleValidation.validationSuite(dto, doctorMail);

        Schedule entity = toEntity(dto);
        Doctor doctor = doctorRepository.findByEmail(doctorMail);
        entity.setDoctor(doctor);

        entity = scheduleRepository.save(entity);

        return toDto(entity);
    }

}
