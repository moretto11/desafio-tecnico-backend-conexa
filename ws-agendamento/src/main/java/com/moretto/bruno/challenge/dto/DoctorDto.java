package com.moretto.bruno.challenge.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DoctorDto implements Serializable {

    private Long id;

    @NotBlank(message = "É preciso informar o e-mail do médico")
    @Email(message = "E-mail com formato inválido")
    private String email;

    @NotBlank(message = "É preciso informar a senha")
    @JsonAlias("senha")
    private String password;

    @NotBlank(message = "É preciso informar a confirmação da senha")
    @JsonAlias("confirmacaoSenha")
    private String confirmationPassword;

    @NotBlank(message = "É preciso informar a especialidade do médico")
    @JsonAlias("especialidade")
    private String speciality;

    @NotBlank(message = "É preciso informar o CPF do médico")
    @Size(min = 14, max = 14, message = "CPF com formato inválido. O formato deve ser 000.000.000-00")
    private String cpf;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull(message = "É preciso informar a data de nascimento do médico")
    @JsonAlias("dataNascimento")
    private LocalDate birthDate;

    @NotBlank(message = "É preciso informar o telefone do médico")
    @Size(min = 14, max = 14, message = "Telefone com formato inválido. O formato deve ser (00) 00000-0000")
    @JsonAlias("telefone")
    private String phone;

}
