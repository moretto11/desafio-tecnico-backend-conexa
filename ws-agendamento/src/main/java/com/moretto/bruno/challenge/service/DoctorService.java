package com.moretto.bruno.challenge.service;

import com.moretto.bruno.challenge.dto.DoctorDto;
import com.moretto.bruno.challenge.exception.ValidationException;

public interface DoctorService {

    /**
     * Salva um novo médico na base
     * @param dto Dados do médico
     * @return DTO do médico salvo
     * @throws ValidationException É lançada caso tenha algum dado inválido
     */
    DoctorDto save(DoctorDto dto) throws ValidationException;

}
