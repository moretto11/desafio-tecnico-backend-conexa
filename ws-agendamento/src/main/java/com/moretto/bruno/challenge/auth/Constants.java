package com.moretto.bruno.challenge.auth;

public class Constants {

    private Constants() {
    }

    public static final String SECRET = "CHALLENGE_AUTH";
    public static final long EXPIRATION_TIME = 900_000;
    public static final String SIGN_UP_URL = "/v1/signup";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";


}
